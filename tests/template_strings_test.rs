extern crate template_strings;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[test]
fn new_template_string() {
    let tp = "{{ test }}";

    let template = template_strings::TemplateString::new(tp);
    assert_eq!(&template.template, &tp);
}

#[test]
fn new_template_string_from_file() {
    let path = "./tests/fixtures/test_template.txt";
    let template = template_strings::TemplateString::new_from_file(path);

    let f = File::open(path).expect("error opening file");
    let reader = BufReader::new(f);
    let mut buffer = String::new();

    for line in reader.lines() {
        buffer.push_str(&(line.expect("error reading line"))[..]);
    }

    assert_eq!(&template.template, &buffer);
}
