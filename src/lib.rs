// template_string
// Copyright (C) 2018  Ciro DE CARO
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#![feature(test)]
#![feature(repeat_generic_slice)]
extern crate test;
extern crate regex;
#[macro_use]
extern crate lazy_static;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use regex::Regex;
use std::collections::HashMap;

lazy_static! {
    static ref macro_regex: Regex = Regex::new(r"\{\{([^}]*)\}\}").unwrap();
}


pub struct TemplateString {
    pub template: String,
    matches: Vec<(usize, usize)>,
}

impl TemplateString {
    pub fn new(template: &str) -> Self {
        TemplateString {
            template: template.to_owned(),
            matches: macro_regex
                .find_iter(template)
                .map(|m| (m.start(), m.end()))
                .collect(),
        }
    }
    pub fn new_from_file(path_string: &str) -> Self {
        let f = File::open(path_string).expect("error opening file");
        let reader = BufReader::new(f);
        let mut buffer = String::new();
        
        for line in reader.lines() {
            buffer.push_str(&(line.expect("error reading line"))[..]);
        }
        
        TemplateString {
            template: String::from(&buffer[..]),
            matches: macro_regex
                .find_iter(&buffer[..])
                .map(|m| (m.start(), m.end()))
                .collect(),
        }
    } 
    pub fn render(&self, macros: HashMap<&str, &str>) -> String {
        self.render_named(macros)
    }

    pub fn render_named(&self, macros: HashMap<&str, &str>) -> String {
        let mut parts: Vec<&str> = vec![];
        let template_str = &self.template;

        let total_len = template_str.len();
        let mut next_arg_index = 0;
        let mut cursor = 0;

        while cursor < total_len {
            // next text to replace position
            let next_arg = self.matches[next_arg_index];

            if cursor >= next_arg.0 && cursor <= next_arg.1 {
                // get argument and its name based on the braces {{}}
                let arg = &template_str[next_arg.0..next_arg.1];
                let arg_name = &arg[2..arg.len() - 2];
                // if argument matches exists in macros
                // push it into the parts array for later reconstruction
                // else push the unreplaced template string
                match macros.get(arg_name.trim()) {
                    Some(s) => parts.push(s),
                    _ => parts.push(arg),
                }

                cursor += arg.len();
                next_arg_index += 1;
            } else {
                parts.push(&template_str[cursor..cursor+1]);
                cursor += 1; 
            }

        }

        parts.join("")
    }

    pub fn render_ordinal(&self, macros: Vec<&str>) -> String {
        let mut parts: Vec<&str> = vec![];
        let template_str = &self.template;

        let total_len = template_str.len();
        let mut next_arg_index = 0;
        let mut cursor = 0;

        while cursor < total_len {
            // next text to replace position
            let next_arg = self.matches[next_arg_index];

            if cursor >= next_arg.0 && cursor <= next_arg.1 {
                // get argument and its name based on the braces {{}}
                let arg = &template_str[next_arg.0..next_arg.1];

                let replacement: Vec<&str> = macros.clone()
                    .drain(next_arg_index..next_arg_index + 1)
                    .collect();

                if replacement.len() == 1 {
                    parts.push(&macros[next_arg_index]);
                } else {
                    parts.push(&arg);
                }

                cursor += arg.len();
                next_arg_index += 1;
            } else {
                parts.push(&template_str[cursor..cursor+1]);
                cursor += 1; 
            }

        }

        parts.join("")
    }
}

#[cfg(test)]
mod template_strings_tests {
    use std::collections::HashMap;
    use super::*;
    #[test]
    fn new_template_string() {
        let tp = "{{ test }} {{ lol }} \n {{ test }}";
        let v: Vec<(usize, usize)> = vec![(0, 10), (11, 20), (23, 33)];

        let template = TemplateString::new(tp);
        assert_eq!(&template.template, &tp);
        assert_eq!(&template.matches, &v);
    }

    #[test]
    fn render_template_string() {
        let tp = "Je m'appelle {{name}} {{surname}} \n {{desc}}";

        let template = TemplateString::new(tp);
        let mut macros = HashMap::new();
        macros.insert(
            "name",
            "Ciro",
            );
        macros.insert(
            "surname",
            "DE CARO",
            );
        macros.insert(
            "desc",
            "je suis un développeur",
            );
        assert_eq!(template.render(macros), "Je m'appelle Ciro DE CARO \n je suis un développeur");
    }
    #[test]
    fn render_ordinal_template_string() {
        let tp = "Je m'appelle {{}} {{}} \n {{}}";

        let template = TemplateString::new(tp);
        let macros: Vec<&str> = vec!["Ciro", "DE CARO", "je suis un développeur"];
        assert_eq!(template.render_ordinal(macros), "Je m'appelle Ciro DE CARO \n je suis un développeur");
    } 
    #[test]
    fn render_template_string_from_file() {
        let path = "./tests/fixtures/test_template.txt";
        let tp = TemplateString::new_from_file(path);

        let mut macros = HashMap::new();
        macros.insert(
            "test",
            "texte",
            );
        macros.insert(
            "lignes",
            "bateaux de croisières",
            );
        macros.insert(
            "code",
            "function test() { return 'lol'}",
            );
        println!("{}", &tp.render(macros.clone()));
        assert_eq!(tp.render(macros), "je suis un texte un peu plus long\
                   Avec plusieurs bateaux de croisières.\
                   
                   function test() { return 'lol'}");
    }
}

#[cfg(test)]
mod template_strings_bench {
    use test::Bencher;
    use super::*;
    use std::collections::HashMap;

    fn new_template_string_bench(tp: &str) {
        let template = TemplateString::new(tp);
        assert_eq!(&template.template, &tp);
    }
    
    fn render_template_string_bench(repeat: usize) {
        let tp = &("Je m'appelle {{name}} {{surname}} \n {{desc}}".repeat(repeat));

        let template = TemplateString::new(tp);
        let mut macros = HashMap::new();
        macros.insert(
            "name",
            "Ciro",
        );
        macros.insert(
            "surname",
            "DE CARO",
        );
        macros.insert(
            "desc",
            "je suis un développeur",
        );
        assert_eq!(template.render(macros), "Je m'appelle Ciro DE CARO \n je suis un développeur".repeat(repeat));
    }
    fn render_ordinal_template_string_bench(repeat: usize) {
        let tp = &("Je m'appelle {{}} {{}} \n {{}}".repeat(repeat));

        let template = TemplateString::new(tp);
        let macros: Vec<&str> = (vec!["Ciro", "DE CARO", "je suis un développeur"])[..].repeat(repeat);
        assert_eq!(template.render_ordinal(macros), "Je m'appelle Ciro DE CARO \n je suis un développeur".repeat(repeat));
    }

    #[bench]
    fn x1_bench_new_template_string(b: &mut Bencher) {
        b.iter(|| new_template_string_bench("{{ test }}"));
    }
    #[bench]
    fn x10_bench_new_template_string(b: &mut Bencher) {
        b.iter(|| new_template_string_bench(&("{{ test }}".repeat(10))));
    }
    #[bench]
    fn x100_bench_new_template_string(b: &mut Bencher) {
        b.iter(|| new_template_string_bench(&("{{ test }}".repeat(100))));
    }
    #[bench]
    fn x1000_bench_new_template_string(b: &mut Bencher) {
        b.iter(|| new_template_string_bench(&("{{ test }}".repeat(1000))));
    }
    #[bench]
    fn x10000_bench_new_template_string(b: &mut Bencher) {
        b.iter(|| new_template_string_bench(&("{{ test }}".repeat(10000))));
    }

    #[bench]
    fn x1_bench_render_template_string(b: &mut Bencher) {
        b.iter(|| render_template_string_bench(0));
    }
    #[bench]
    fn x10_bench_render_template_string(b: &mut Bencher) {
        b.iter(|| render_template_string_bench(10));
    }
    #[bench]
    fn x100_bench_render_template_string(b: &mut Bencher) {
        b.iter(|| render_template_string_bench(100));
    }
    #[bench]
    fn x1000_bench_render_template_string(b: &mut Bencher) {
        b.iter(|| render_template_string_bench(1000));
    }
    #[bench]
    fn x10000_bench_render_template_string(b: &mut Bencher) {
        b.iter(|| render_template_string_bench(10000));
    }

    #[bench]
    fn x1_bench_render_ordinal_template_string(b: &mut Bencher) {
        b.iter(|| render_ordinal_template_string_bench(0));
    }
    #[bench]
    fn x10_bench_render_ordinal_template_string(b: &mut Bencher) {
        b.iter(|| render_ordinal_template_string_bench(10));
    }
    #[bench]
    fn x100_bench_render_ordinal_template_string(b: &mut Bencher) {
        b.iter(|| render_ordinal_template_string_bench(100));
    }
    #[bench]
    fn x1000_bench_render_ordinal_template_string(b: &mut Bencher) {
        b.iter(|| render_ordinal_template_string_bench(1000));
    }
    // can't run ...
    // memory problems due to repeat on real Vec i think?
    // @TODO find a way to circumvent to .repeat() in test
    // #[bench]
    // fn x10000_bench_render_ordinal_template_string(b: &mut Bencher) {
    //     b.iter(|| render_ordinal_template_string_bench(10000));
    // }
}

